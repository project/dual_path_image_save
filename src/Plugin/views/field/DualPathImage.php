<?php

namespace Drupal\dual_path_image_save\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\Core\Form\FormStateInterface;

/**
 * Field handler to show the image from the custom path.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("dual_path_image")
 */
class DualPathImage extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['image_field'] = ['default' => ''];
    $options['image_style'] = ['default' => ''];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    $selected_entity_type = $this->view->storage->get('base_table'); // Get base table

    // Manually mapping base table to entity type
    $entity_type_mapping = [
      'node_field_data' => 'node',
      'profile' => 'profile',
    ];

    if (isset($entity_type_mapping[$selected_entity_type])) {
      $entity_type = $entity_type_mapping[$selected_entity_type];
    } else {
      $entity_type = 'node'; // Fallback
    }

    $field_configs = \Drupal::entityTypeManager()->getStorage('field_config')->loadByProperties(['entity_type' => $entity_type, 'field_type' => 'image']);
    $fields_with_custom_path = [];

    foreach ($field_configs as $field_config) {
      $settings = $field_config->getThirdPartySettings('dual_path_image_save');
      if (isset($settings['path']) && !empty($settings['path'])) {
        $fields_with_custom_path[$field_config->getName()] = $field_config->getLabel() . ' (' . $field_config->getName() . ')';
      }
    }

    $form['image_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Image Field'),
      '#options' => $fields_with_custom_path,
      '#default_value' => $this->options['image_field'],
    ];

    // Image styles.
    $styles = image_style_options(FALSE);
    $form['image_style'] = [
      '#type' => 'select',
      '#title' => $this->t('Image Style'),
      '#options' => $styles,
      '#default_value' => $this->options['image_style'],
    ];

    parent::buildOptionsForm($form, $form_state);
  }


public function render(ResultRow $values) {
  $base_table = $this->view->storage->get('base_table');

  $entity_type_mapping = [
    'node_field_data' => 'node',
    'profile' => 'profile',
  ];

  if (isset($entity_type_mapping[$base_table])) {
    $entity_type = $entity_type_mapping[$base_table];
  } else {
    $entity_type = 'node'; // Fallback
  }

  $field_name = $this->options['image_field'];
  $original_uri = $values->_entity->{$field_name}->entity->uri->value;

  $bundle = $values->_entity->bundle();
  $field_config = \Drupal\field\Entity\FieldConfig::loadByName($entity_type, $bundle, $field_name);
  $third_party_settings = $field_config->getThirdPartySettings('dual_path_image_save');
  $custom_path = isset($third_party_settings['path']) ? $third_party_settings['path'] : 'public://';

  $filename = \Drupal::service('file_system')->basename($original_uri);
  $custom_uri = $custom_path . '/' . $filename;

  // Create the directory if it does not exist.
  $fileSystem = \Drupal::service('file_system');
  $directory = $fileSystem->dirname($custom_uri);

    // Create the directory if it does not exist.
    if ($fileSystem->prepareDirectory($directory, \Drupal\Core\File\FileSystemInterface::CREATE_DIRECTORY)) {
      // Copy the file to the custom directory
      if ($fileSystem->copy($original_uri, $custom_uri, \Drupal\Core\File\FileSystemInterface::EXISTS_REPLACE)) {
        // File successfully copied to custom path
      }
      else {
        \Drupal::logger('dual_path_image_save')->warning('Could not copy file to @dir.', ['@dir' => $directory]);
      }
    }
    else {
      \Drupal::logger('dual_path_image_save')->warning('Could not prepare directory @dir for images.', ['@dir' => $directory]);
    }


  $style_name = $this->options['image_style'];
  if (!empty($style_name) && $style = \Drupal::entityTypeManager()->getStorage('image_style')->load($style_name)) {
    $custom_uri = $style->buildUrl($custom_uri);
  }

  return [
    '#theme' => 'image',
    '#uri' => $custom_uri,
    '#alt' => $values->_entity->{$field_name}->alt,
  ];
}


  /**
   * {@inheritdoc}
   */
  public function query() {
    // We don't query the database for this field.
    return;
  }
}

