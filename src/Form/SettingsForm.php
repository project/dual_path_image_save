<?php

namespace Drupal\dual_path_image_save\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class SettingsForm extends ConfigFormBase {

  protected function getEditableConfigNames() {
    return ['dual_path_image_save.settings'];
  }

  public function getFormId() {
    return 'dual_path_image_save_admin_settings';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('dual_path_image_save.settings');
    $form['fields'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Fields'),
      '#default_value' => $config->get('fields'),
      '#description' => $this->t('Enter one field name per line for which you want to enable the dual path save feature. For example, field_custom_image.'),
    ];
    return parent::buildForm($form, $form_state);
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->config('dual_path_image_save.settings')
      ->set('fields', $form_state->getValue('fields'))
      ->save();
  }
}

