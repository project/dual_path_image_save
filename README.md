# Dual Path Image Save

## Overview
The "Dual Path Image Save" module for Drupal 9 offers a unique functionality to save image fields in two separate locations - the default directory and a custom directory specified by the users. This is handy in scenarios where redundancy or parallel processing of uploaded images is required.

## Features
- **Dual Directory Save**: Every image uploaded through specified fields will be saved in both its default location and a custom directory.
- **Field Specific Configuration**: The dual-save feature can be enabled on a per-field basis. Not all image fields have to be saved in two places, granting flexibility.
- **Easy to Configure**: Via the field settings, administrators can easily set up the custom directory for each image field.

## Requirements
- Drupal 9 core
- Image module (from Drupal core)

## Installation and Configuration
1. **Installation**:
   - Download and place the "Dual Path Image Save" module in the `sites/all/modules/custom` directory.
   - Enable the module either through the Drupal admin interface or using Drush (`drush en dual_path_image_save`).

2. **Configuration**:
   - Navigate to the settings of an image field where you want to enable the dual-save feature.
   - Under 'Dual Path Settings', specify the custom directory where you'd like the images to be saved in addition to the default location.
   - Save your settings.

## Support and Maintenance
For any bugs, feature requests, or patches, please use the issue queue. Your feedback and contributions are highly appreciated!

