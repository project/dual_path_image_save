<?php

/**
 * Implements hook_views_data_alter().
 */
function dual_path_image_save_views_data_alter(array &$data) {
  $data['node_field_data']['dual_path_image'] = [
    'title' => t('Dual Path Image'),
    'help' => t('Display the image from the custom path.'),
    'field' => [
      'id' => 'dual_path_image',
    ],
  ];
  
  $data['profile']['dual_path_image'] = [
    'title' => t('Dual Path Image (Profile)'),
    'field' => [
      'id' => 'dual_path_image',
    ],
  ];
}
